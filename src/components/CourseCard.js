import { Row, Col, Card, Button } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function Highlights({courseProp}) {
	// console.log(courseProp);
	// expected result is coursesData[0]
	// console.log(typeof courseProp);
	// result: object

	// const [count, setCount] = useState(0);
	// const [seat, setSeat] = useState(30);
	// const [isOpen, setIsOpen] = useState(true);
	// // useState():
	// // syntax: const [getter, setter] = useState(initialValueOfGetter)

	// function enroll (){
		
	// 	if (seat > 0) {
	// 		setCount(count + 1);
	// 		setSeat(seat - 1);
	// 	}
	// }

	// useEffect(() => {
	// 	if (seat === 0){
	// 		alert(`No more seats available for ${name}`)
	// 	}
	// }, [seat])
	// useEffect():
	// syntax: useEffect(() => {}, [optionalParameter])

	const {name, description, price, _id} = courseProp;

	return (

		<Row className="mt-3 mb-3">

			<Col xs={12} md={12}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<h5>Description:</h5>
						<p>{description}</p>
						<h5>Price:</h5>
						<p>{price}</p>
						<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
					</Card.Body>
				</Card>
			</Col>
			
		</Row>
	)
}