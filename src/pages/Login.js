import {useState, useEffect, useContext} from 'react'
import {Navigate} from 'react-router-dom'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login (props) {

	// allows us to consume the User context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext)

	const [email, setLogin] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	// console.log(`user ${user.id}`)


	useEffect(() => {

		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false)
		}

	}, [email, password, setIsActive])
	

	function authenticate(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if (typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			} else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}

		})

		setLogin('');
		setPassword('');

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details',{
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				// console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})

			})
		}

		// localStorage.setItem("email", email)

		// to acces the user information, it can be done using localStorage, this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		// when state change components are rerendered and the AppNavbar component will be updates base on the user credential
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// set the email of the authenticated user in the local storage
		// Syntax: localStorage.setItem("key", value)
		
		// alert(`\nYou are now logged in!\n\nEmail: ${email}`)
	}

	return (

		(user.id !== null) ?

		<Navigate to ="/courses"/>
		

		:

		<Form onSubmit={e => authenticate(e)}>
		<h1>Login</h1>
			<Form.Group controlId="email">
				<Form.Label>Login</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter Email"
					value = {email}
					onChange = {e => setLogin(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Enter Password"
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{	isActive ?
				<Button type="submit" variant="primary" className="my-2">
					Login
				</Button>
				:
				<Button type="submit" variant="primary" className="my-2" disabled>
					Login
				</Button>
			}

		</Form>
	);
}