import {Form, Button} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2'


export default function Register() {

	const {user} = useContext(UserContext);

	const history = useNavigate()

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	// console.log(`email is ${email}`)
	// console.log(`password1 is ${password1}`)
	// console.log(`password2 is ${password2}`)

	// simulation of user registration

	function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true){
				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Email already exist"
				})
			} else {

				fetch('http://localhost:4000/users/register',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					
					console.log("data");
					console.log(data);

					if (data === true){
						setFirstName('');
						setLastName('');
						setMobileNo('');
						setEmail('');
						setPassword('');

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "You can now login"
						})

						history("/login")
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
				})
			}
		})
	}

	useEffect(() => {

		if ( (email !== '' && password !== '' && firstName !== '' && lastName !== '' && mobileNo !== '') ) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password,])

	// console.log("user.token")
	// console.log(user.token)

	return (
	
		(user.id !== null) ?

			<Navigate to='/courses'/>
	
		:

		<Form onSubmit={e => registerUser(e)}>
		<h1>Register</h1>

			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type = "text"
					placeholder = "Enter First Name here"
					value = {firstName}
					onChange = {e => setFirstName (e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type = "text"
					placeholder = "Enter Last Name here"
					value = {lastName}
					onChange = {e => setLastName (e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type = "text"
					placeholder = "Enter Mobile Number here"
					value = {mobileNo}
					onChange = {e => setMobileNo (e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder= "Enter Email here"
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Enter Password here"
					value = {password}
					onChange = {e => setPassword (e.target.value)}
					required
				/>
			</Form.Group>

		{/*Ternart Operator (?:)
			? - if
			: - else
		*/}

			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="my-2">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="my-2" disabled>
					Submit
				</Button>
			}

		</Form>

	);
};